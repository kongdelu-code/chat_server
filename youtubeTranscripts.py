'''
Author: caoniu caoniu@jushuitan.com
Date: 2023-06-08 18:49:49
LastEditors: caoniu caoniu@jushuitan.com
LastEditTime: 2023-06-08 18:58:06
FilePath: /chat_server/youtubeTranscripts.py
Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
'''
# !pip install youtube-transcript-api
# !pip install langchain
# !pip install pytube
# "https://www.youtube.com/watch?v=rFQ5Kmkd4jc"


from langchain.document_loaders import YoutubeLoader
import sys
args = sys.argv
if len(args) > 1:
    loader = YoutubeLoader.from_youtube_url(args[1], add_video_info=True, language=['zh-Hans','id'], translation='zh-Hans')
    result = loader.load()
    print(result[0].page_content)
