#!/usr/bin/env python3

"""
Basic example of edge_tts usage.
"""

import asyncio
import edge_tts

VOICE = "zh-CN-XiaoyiNeural"
OUTPUT_FILE = "./static/voice_output.mp3"

import sys

async def _main() -> None:
    args = sys.argv[1:]
    if len(args) > 0:
        communicate = edge_tts.Communicate(args[0], VOICE)
        await communicate.save(args[1])

if __name__ == "__main__":
    # loop = asyncio.get_event_loop() 已弃用
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    try:
        asyncio.run(_main())
    except KeyboardInterrupt:
        pass