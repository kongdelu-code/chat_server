const express = require('express');
const app = express();

app.get('/', (req, res) => {
    res.setHeader('Content-Type', 'text/html');
    res.setTimeout(60*1000, function(){
        console.log("Request has timed out.");
        return res.status(408).send("Request timeout.")
    })
    res.sendFile(__dirname + "/aiChat.html");
});

app.listen(3000, () => {
console.log('Server running at http://localhost:3000/');
});
